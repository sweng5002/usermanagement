let mocha = require('mocha'),
    chai = require('chai'),
    expect = require('chai').expect,
    Seneca = require('seneca'),
    cache = require('cache')();

let username = "test" + Math.floor((Math.random() * 123456789 ));
let password = 'testPass123';

function test_seneca(fin) {
    return Seneca({log: 'test'})
        .test(fin)
        .use(require('../userManagement'))
}

describe('userManagement registration', function () {
    this.timeout(10000);

    // set the input for the microservice call
    let input = {
        service: 'userManagement',
        operation: 'register',
        body: {username: username, password: password, email: 'testEmail@email.com'}
    };

    // test happy path
    it('should return success after registering user', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(1);
            fin();
        })
    });

    // test duplicate username
    it('should not allow the same username for more than one account', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            expect(data.success).to.equal(0);
            fin();
        })
    });

    // test username = password
    it('should not allow the username to equal the password', function (fin) {
        const seneca = test_seneca(fin);

        // generate a new random username
        let differentUsername = "test" + Math.floor((Math.random() * 123456789 ));

        let input = {
            service: 'userManagement',
            operation: 'register',
            body: {username: differentUsername, password: differentUsername, email: 'testEmail@email.com'}
        };

        seneca.act(input, function (ignore, data) {
            expect(data.success).to.equal(0);
            fin();
        })
    });
});

describe('userManagement login', function () {
    this.timeout(10000);

    // set the input for the microservice call
    let input = {
        service: 'userManagement',
        operation: 'login',
        body: {username: username, password: password}
    };

    // test happy path
    it('should login with registered username', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            console.log("\n\n userManagement login got data: ", data);

            expect(data.success).to.equal(1);
            // expect(data.result.username).to.equal(username);
            fin();
        })
    });

    // test incorrect username path
    it('should not login with non registered username', function (fin) {
        const seneca = test_seneca(fin);

        input.body.username = 'wrongUser';
        seneca.act(input, function (ignore, data) {
            console.log("\n\n userManagement login got data: ", data);

            expect(data.success).to.equal(0);
            fin();
        })
    });


    // test incorrect password path
    it('should not login with incorrect password', function (fin) {
        const seneca = test_seneca(fin);

        input.body.username = username;
        input.body.password = 'wrongPass';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n userManagement login got data: ", data);

            expect(data.success).to.equal(0);
            fin();
        })
    });

    // test missing username
    it('should not login with no username', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.username;
        input.body.password = password;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n userManagement login got data: ", data);

            expect(data.success).to.equal(0);
            fin();
        })
    });

    // test missing password
    it('should not login with no password', function (fin) {
        const seneca = test_seneca(fin);

        input.body.username = username;
        delete input.body.password;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n userManagement login got data: ", data);

            expect(data.success).to.equal(0);
            fin();
        })
    });
    input.body.password = password;
});

describe('userManagement getStates', function () {
    this.timeout(10000);

    // set the input for the microservice call
    let input = {
        service: 'userManagement',
        operation: 'getStates'
    };

    // test happy path
    it('should return success with list of states', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(1);
            expect(data.result.states.length).to.equal(51);
            fin();
        })
    });
});

describe('userManagement addAddress', function () {
    this.timeout(10000);

    // set the input for the microservice call
    let input = {
        service: 'userManagement',
        operation: 'addAddress',
        headers: {username: username},
        body: {name: 'name', line1: 'line1', line2: "line2", city: "city", state: 'Alaska', zip: 12345}
    };

    // test happy path
    it('should return success and address after adding address', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(1);
            expect(data.result.address).to.equal(input.body);
            fin();
        })
    });

    // test invalid zip
    it('should fail when the zip is letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.zip = 'badzip';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.zip = 12345;

            fin();
        })
    });

    it('should fail when the zip is 4 numbers', function (fin) {
        const seneca = test_seneca(fin);

        input.body.zip = 1234;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.zip = 12345;
            fin();
        })
    });

    it('should fail when the state is not one of the 50 + DC', function (fin) {
        const seneca = test_seneca(fin);

        input.body.state = 'Timbuctu';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.state = "Alaska";
            fin();
        })
    });

    it('should fail when the user isn\'t logged in', function (fin) {
        const seneca = test_seneca(fin);

        input.headers.username = '';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.headers.username = username;

            fin();
        })
    });
});


describe('userManagement addBankAccount', function () {
    this.timeout(10000);

    // set the input for the microservice call
    let input = {
        service: 'userManagement',
        operation: 'addBankAccount',
        headers: {username: username},
        body: {accountNumber: 123456, routingNumber: 123456789}
    };

    // test happy path
    it('should return success', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(1);
            fin();
        })
    });

    // test invalid accountNumber
    it('should fail when the account number is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.accountNumber = 12345;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.accountNumber = 123456;
            fin();
        })
    });

    it('should fail when the accountNumber is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.accountNumber = 1234567890123456789012;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.accountNumber = 123456;
            fin();
        })
    });

    it('should fail when the accountNumber has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.accountNumber = '1234five6';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.accountNumber = 123456;
            fin();
        })
    });

    // test invalid routingNumber
    it('should fail when the routing number is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.routingNumber = 12345678;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.routingNumber = 123456789;
            fin();
        })
    });

    it('should fail when the routingNumber is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.routingNumber = 1234567890;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.routingNumber = 123456789;
            fin();
        })
    });

    it('should fail when the routingNumber has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.routingNumber = '1234five6';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.routingNumber = 123456789;
            fin();
        })
    });

    it('should fail when the routingNumber is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.routingNumber;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.routingNumber = 123456789;
            fin();
        })
    });

    it('should fail when the accountNumber is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.accountNumber;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.accountNumber = 123456;
            fin();
        })
    });
});


describe('userManagement addCreditCard', function () {
    this.timeout(10000);

    // set the input for the microservice call
    let input = {
        service: 'userManagement',
        operation: 'addCreditCard',
        headers: {username: username},
        body: {creditCardNumber: 1234567890123456, expirationDate: '01/2021', cvv: 123}
    };

    // test happy path
    it('should return success', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(1);
            fin();
        })
    });

    // test invalid creditCardNumber
    it('should fail when the credit card number is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.creditCardNumber = 12345;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    it('should fail when the creditCardNumber is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.creditCardNumber = 1234567890123456789012;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    it('should fail when the creditCardNumber has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.creditCardNumber = '1234five90123456';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    it('should fail when the creditCardNumber is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.creditCardNumber;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    // test invalid cvv
    it('should fail when the cvv is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.cvv = 12;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    it('should fail when the cvv is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.cvv = 1234;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    it('should fail when the cvv has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.cvv = 'E12';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    it('should fail when the cvv is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.cvv;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    // test invalid expirationDate
    it('should fail when the expirationDate has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = 'mm/yyyy';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate has letters in just the month', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = 'mm/01';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate has letters in just the year', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '01/yyyy';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '1';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '01/20211';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate month is less than 1', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '00/2001';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate month is greater than 12', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '13/2001';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.expirationDate;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });
});


describe('userManagement addCreditCardAndAddress', function () {
    this.timeout(10000);

    // set the input for the microservice call
    let input = {
        service: 'userManagement',
        operation: 'addCreditCardAndAddress',
        headers: {username: username},
        body: {
            creditCardNumber: 1234567890123456, expirationDate: '01/2021', cvv: 123,
            name: 'name', line1: 'line1', line2: "line2", city: "city", state: 'Alaska', zip: 12345
        }
    };

    // test happy path
    it('should return success', function (fin) {
        const seneca = test_seneca(fin);

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(1);
            fin();
        })
    });


    // test invalid zip
    it('should fail when the zip is letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.zip = 'badzip';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.zip = 12345;

            fin();
        })
    });

    it('should fail when the zip is 4 numbers', function (fin) {
        const seneca = test_seneca(fin);

        input.body.zip = 1234;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.zip = 12345;
            fin();
        })
    });

    it('should fail when the state is not one of the 50 + DC', function (fin) {
        const seneca = test_seneca(fin);

        input.body.state = 'Timbuctu';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.state = "Alaska";
            fin();
        })
    });

    it('should fail when the user isn\'t logged in', function (fin) {
        const seneca = test_seneca(fin);

        input.headers.username = '';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.headers.username = username;

            fin();
        })
    });

    // test invalid creditCardNumber
    it('should fail when the credit card number is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.creditCardNumber = 12345;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    it('should fail when the creditCardNumber is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.creditCardNumber = 1234567890123456789012;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    it('should fail when the creditCardNumber has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.creditCardNumber = '1234five90123456';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    it('should fail when the creditCardNumber is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.creditCardNumber;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.creditCardNumber = 1234567890123456;
            fin();
        })
    });

    // test invalid cvv
    it('should fail when the cvv is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.cvv = 12;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    it('should fail when the cvv is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.cvv = 1234;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    it('should fail when the cvv has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.cvv = 'E12';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    it('should fail when the cvv is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.cvv;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.cvv = 123;
            fin();
        })
    });

    // test invalid expirationDate
    it('should fail when the expirationDate has letters', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = 'mm/yyyy';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate has letters in just the month', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = 'mm/01';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate has letters in just the year', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '01/yyyy';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate is too short', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '1';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate is too long', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '01/20211';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate month is less than 1', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '00/2001';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate month is greater than 12', function (fin) {
        const seneca = test_seneca(fin);

        input.body.expirationDate = '13/2001';

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });

    it('should fail when the expirationDate is missing', function (fin) {
        const seneca = test_seneca(fin);

        delete input.body.expirationDate;

        seneca.act(input, function (ignore, data) {
            console.log("\n\n got data: ", data);

            expect(data.success).to.equal(0);
            input.body.expirationDate = '01/2021';
            fin();
        })
    });
});