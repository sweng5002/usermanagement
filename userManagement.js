var path = require('path'),
    logging = require('logging')(),
    services = require('services')(),
    generalLogger = logging.general,
    transactionLogger = logging.transaction,
    logTypes = logging.logTypes,
    config = require(path.resolve('./utilities/config.js'))(),
    baseurl = config.baseurl,
    cache = require('cache')(),
    Promise = require('bluebird');

const states = ["Alaska",
    "Alabama",
    "Arkansas",
    "Arizona",
    "California",
    "Colorado",
    "Connecticut",
    "District of Columbia",
    "Delaware",
    "Florida",
    "Georgia",
    "Hawaii",
    "Iowa",
    "Idaho",
    "Illinois",
    "Indiana",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Massachusetts",
    "Maryland",
    "Maine",
    "Michigan",
    "Minnesota",
    "Missouri",
    "Mississippi",
    "Montana",
    "North Carolina",
    "North Dakota",
    "Nebraska",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "Nevada",
    "New York",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Virginia",
    "Vermont",
    "Washington",
    "Wisconsin",
    "West Virginia",
    "Wyoming"];


module.exports = function userManagement(options) {
    var seneca = this;

    // console.log(baseurl);

    seneca.add({init: 'userManagement'}, init);
    seneca.add({service: 'userManagement', operation: 'register'}, registerNewUser);
    seneca.add({service: 'userManagement', operation: 'addAddress'}, addAddress);
    seneca.add({service: 'userManagement', operation: 'getStates'}, getStates);
    seneca.add({service: 'userManagement', operation: 'test'}, test);
    seneca.add({service: 'userManagement', operation: 'login'}, login);
    seneca.add({service: 'userManagement', operation: 'addCreditCard'}, addCreditCard);
    seneca.add({service: 'userManagement', operation: 'addCreditCardAndAddress'}, addCreditCardAndAddress);
    seneca.add({service: 'userManagement', operation: 'addBankAccount'}, addBankAccount);


    function init(msg, respond) {
        // Can add logging stuff here
        respond();
    };
}

function addCreditCardAndAddress(req, done) {
    console.log("addCreditCardAndAddress called");

    if (req && req.body && req.headers) {

        // this action requires the user be logged in.  check for the username header
        loginValid(req.headers.username, done)
            .then(function (result) {
                if (!result) {
                    userNotLoggedInError(done);
                    return;
                } else {
                    console.log("addCreditCardAndAddress got req.body: ", req.body);

                    // check that all the required fields were sent in the request body
                    const requiredFields = ["creditCardNumber", "cvv", "expirationDate", "name", "line1", "line2", "city", "state", "zip"];
                    const receivedFields = Object.getOwnPropertyNames(req.body);

                    var missingFields = false;
                    requiredFields.forEach(function (requiredField) {
                        if (receivedFields.indexOf(requiredField) === -1) {
                            missingRequiredFieldsError(requiredFields, done);
                            missingFields = true;
                            return;
                        }
                    })
                    if (missingFields) {
                        return;
                    }

                    let validationMessage = '';

                    // make sure the state is a real state
                    if (!stateValid(req.body.state)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The state must be one of the 50 US states. " + req.body.state + " is not a valid state."
                    }

                    // make sure the zip meets the length/format requirements
                    if (!zipValid(req.body.zip)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The zip requires 5 numeric characters"
                    }

                    // make sure the creditCardNumber is valid
                    if (!creditCardNumberValid(req.body.creditCardNumber)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The credit card number must be a 16 digit number. " + req.body.creditCardNumber + " is not valid."
                    }

                    // make sure the cvv is valid
                    if (!cvvValid(req.body.cvv)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The cvv must be a 3 digit number.  " + req.body.cvv + " is not valid."
                    }

                    // make sure the expiration date is valid
                    if (!expirationDateValid(req.body.expirationDate)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The expiration date must be a future date in MM/YYYY format. " + req.body.expirationDate + " is not valid."
                    }


                    // if any of the input validation failed then error
                    if (validationMessage.length > 0) {
                        invalidInputError(done, validationMessage);
                        return;
                    } else {
                        console.log("input validation passed");
                    }

                    Promise.all([
                        cache.cacheWrite(req.headers.username, "creditCardNumber", req.body.creditCardNumber),
                        cache.cacheWrite(req.headers.username, "cvv", req.body.cvv),
                        cache.cacheWrite(req.headers.username, "expirationDate", req.body.expirationDate),
                        cache.cacheWrite(req.headers.username, "name", req.body.line1),
                        cache.cacheWrite(req.headers.username, "line1", req.body.line1),
                        cache.cacheWrite(req.headers.username, "line2", req.body.line2),
                        cache.cacheWrite(req.headers.username, "city", req.body.city),
                        cache.cacheWrite(req.headers.username, "state", req.body.state),
                        cache.cacheWrite(req.headers.username, "zip", req.body.zip)
                    ])
                        .then(function (results) {
                            console.log("information written to cache with results: ", results);

                            done({
                                success: 1,
                                result: {
                                    message: "payment method and address successfully added to " + req.headers.username
                                }
                            });
                        })
                        .catch(function (err) {
                            cacheWriteError(done);
                        });
                }

            })
            .catch(function (err) {
                console.log("loginValid returned err: ", err);
            })
    } else {
        noBodyError(done);
    }
}

function addBankAccount(req, done) {
    console.log("addBankAccount called");

    if (req && req.body && req.headers) {

        // this action requires the user be logged in.  check for the username header
        loginValid(req.headers.username, done)
            .then(function (result) {
                if (!result) {
                    userNotLoggedInError(done);
                    return;
                } else {
                    console.log("addBankAccount got req.body: ", req.body);

                    // check that all the required fields were sent in the request body
                    const requiredFields = ["accountNumber", "routingNumber"];
                    const receivedFields = Object.getOwnPropertyNames(req.body);

                    var missingFields = false;
                    requiredFields.forEach(function (requiredField) {
                        if (receivedFields.indexOf(requiredField) === -1) {
                            missingRequiredFieldsError(requiredFields, done);
                            missingFields = true;
                            return;
                        }
                    })
                    if (missingFields) {
                        return;
                    }


                    let validationMessage = '';

                    // make sure the routingNumber is valid
                    if (!routingNumberValid(req.body.routingNumber)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The routing number must be a 9 digit number. " + req.body.routingNumber + " is not valid."
                    }

                    // make sure the cvv is valid
                    if (!accountNumberValid(req.body.accountNumber)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The account number must be a 6-20 digit number.  " + req.body.accountNumber + " is not valid."
                    }

                    // if any of the input validation failed then error
                    if (validationMessage.length > 0) {
                        invalidInputError(done, validationMessage);
                        return;
                    } else {
                        console.log("input validation passed");
                    }

                    Promise.all([
                        cache.cacheWrite(req.headers.username, "accountNumber", req.body.accountNumber),
                        cache.cacheWrite(req.headers.username, "routingNumber", req.body.routingNumber),
                    ])
                        .then(function (results) {
                            console.log("account information written to cache with results: ", results);
                            done({
                                success: 1,
                                result: {
                                    message: "payment method successfully added to " + req.headers.username
                                }
                            });
                        })
                        .catch(function (err) {
                            cacheWriteError(done);
                        });
                }
            })
            .catch(function (err) {
                console.log("loginValid returned err: ", err);
            })
    } else {
        noBodyError(done);
    }
}

function addCreditCard(req, done) {
    console.log("addCreditCard called");

    if (req && req.body && req.headers) {

        // this action requires the user be logged in.  check for the username header
        loginValid(req.headers.username, done)
            .then(function (result) {
                if (!result) {
                    userNotLoggedInError(done);
                    return;
                } else {
                    console.log("addCreditCard got req.body: ", req.body);

                    // check that all the required fields were sent in the request body
                    const requiredFields = ["creditCardNumber", "cvv", "expirationDate"];
                    const receivedFields = Object.getOwnPropertyNames(req.body);

                    var missingFields = false;
                    requiredFields.forEach(function (requiredField) {
                        if (receivedFields.indexOf(requiredField) === -1) {
                            missingRequiredFieldsError(requiredFields, done);
                            missingFields = true;
                            return;
                        }
                    })
                    if (missingFields) {
                        return;
                    }


                    let validationMessage = '';

                    // make sure the creditCardNumber is valid
                    if (!creditCardNumberValid(req.body.creditCardNumber)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The credit card number must be a 16 digit number. " + req.body.creditCardNumber + " is not valid."
                    }

                    // make sure the cvv is valid
                    if (!cvvValid(req.body.cvv)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The cvv must be a 3 digit number.  " + req.body.cvv + " is not valid."
                    }

                    // make sure the expiration date is valid
                    if (!expirationDateValid(req.body.expirationDate)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The expiration date must be a future date in MM/YYYY format. " + req.body.expirationDate + " is not valid."
                    }


                    // if any of the input validation failed then error
                    if (validationMessage.length > 0) {
                        invalidInputError(done, validationMessage);
                        return;
                    } else {
                        console.log("input validation passed");
                    }

                    Promise.all([
                        cache.cacheWrite(req.headers.username, "creditCardNumber", req.body.creditCardNumber),
                        cache.cacheWrite(req.headers.username, "cvv", req.body.cvv),
                        cache.cacheWrite(req.headers.username, "expirationDate", req.body.expirationDate),
                    ])
                        .then(function (results) {
                            console.log("credit card information written to cache with results: ", results);

                            if (req.combinedRequest) {
                                return {
                                    success: 1,
                                    result: {
                                        message: "payment method successfully added to " + req.headers.username
                                    }
                                };
                            } else {
                                done({
                                    success: 1,
                                    result: {
                                        message: "payment method successfully added to " + req.headers.username
                                    }
                                });
                            }
                        })
                        .catch(function (err) {
                            cacheWriteError(done);
                        });
                }

            })
            .catch(function (err) {
                console.log("loginValid returned err: ", err);
            })
    } else {
        noBodyError(done);
    }
}

function login(req, done) {
    console.log("login called");

    if (req && req.body) {
        console.log("login got req.body: ", req.body);

        // check that all the required fields were sent in the request body
        const requiredFields = ["username", "password"];
        const receivedFields = Object.getOwnPropertyNames(req.body);

        var missingFields = false;
        requiredFields.forEach(function (requiredField) {
            if (receivedFields.indexOf(requiredField) === -1) {
                missingRequiredFieldsError(requiredFields, done);
                missingFields = true;
                return;
            }
        })
        if (missingFields) {
            return;
        }

        // fetch the password for comparison
        cache.fetchField(req.body.username, "password")
            .then(function (pass) {
                console.log("in login fetchField.then with pass: ", pass);
                if (!pass) {
                    // cache entry not found for username, username doesn't exist in cache
                    usernameNotExistsError(req, done);
                    return;
                } else {

                    // redis returns the string value with double quotes as part of the string, strip them off
                    pass = pass.replace(/"/g, '');

                    // username found, make sure the password matches
                    if (pass == req.body.password) {
                        // user exists with the username and password, login successful
                        done({
                            success: 1,
                            result: {message: "login successful", username: req.body.username}
                        });
                    } else {
                        incorrectPasswordError(done);
                    }
                }
            })
            .catch(function (err) {
                console.log("cacheReadError err: ", err);
                //  cache read error
                cacheReadError(done);
            });

    } else {
        noBodyError(done);
    }
}

function getStates(req, done) {

    done({
        success: 1,
        result: {states: states}
    });
}

function addAddress(req, done) {
    console.log("addAddress got req: ", req);

    if (req && req.body && req.headers) {

        // this action requires the user be logged in.  check for the username header
        loginValid(req.headers.username, done)
            .then(function (result) {
                if (!result) {
                    userNotLoggedInError(done);
                    return;
                } else {

                    // check that all the required fields were sent in the request body
                    const requiredFields = ["name", "line1", "line2", "city", "state", "zip"];
                    const receivedFields = Object.getOwnPropertyNames(req.body);

                    requiredFields.forEach(function (requiredField) {
                        if (receivedFields.indexOf(requiredField) === -1) {
                            missingRequiredFieldsError(requiredFields, done);
                            return;
                        }
                    })

                    let validationMessage = '';

                    // make sure the state is a real state
                    if (!stateValid(req.body.state)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The state must be one of the 50 US states. " + req.body.state + " is not a valid state."
                    }

                    // make sure the zip meets the length/format requirements
                    if (!zipValid(req.body.zip)) {
                        // add a line break if there is already a line in the message
                        if (validationMessage.length > 0) {
                            validationMessage += "\n";
                        }
                        validationMessage += "The zip requires 5 numeric characters"
                    }

                    // if any of the input validation failed then error
                    if (validationMessage.length > 0) {
                        invalidInputError(done, validationMessage);
                        return;
                    } else {
                        console.log("input validation passed");
                    }

                    Promise.all([
                        cache.cacheWrite(req.headers.username, "name", req.body.line1),
                        cache.cacheWrite(req.headers.username, "line1", req.body.line1),
                        cache.cacheWrite(req.headers.username, "line2", req.body.line2),
                        cache.cacheWrite(req.headers.username, "city", req.body.city),
                        cache.cacheWrite(req.headers.username, "state", req.body.state),
                        cache.cacheWrite(req.headers.username, "zip", req.body.zip)
                    ])
                        .then(function (results) {
                            console.log("address written to cache with results: ", results);
                            console.log("got combinedRequest: ", results.combinedRequest);

                            done({
                                success: 1,
                                result: {
                                    message: "address added for " + req.headers.username,
                                    address: req.body

                                }
                            });
                        })
                        .catch(function (err) {
                            cacheWriteError(done);
                        });
                }
            })
            .catch(function (err) {
                console.log("loginValid returned err: ", err);
            })
    } else {
        noBodyError(done);
    }
}

function registerNewUser(req, done) {
    // console.log("registerNewUser got req: ", req);

    if (req && req.body) {

        // check that all the required fields were sent in the request body
        const requiredFields = ["username", "password", "email"];
        const receivedFields = Object.getOwnPropertyNames(req.body);

        requiredFields.forEach(function (requiredField) {
            if (receivedFields.indexOf(requiredField) === -1) {
                missingRequiredFieldsError(requiredFields, done);
                return;
            }
        })

        let validationMessage = '';

        // make sure the username meets the length/format requirements
        if (!usernameValid(req.body.username)) {
            validationMessage += "The username requires at least 5 uppercase, lowercase, OR numeric characters"
        }

        // make sure the password meets the length/format requirements
        if (!passwordValid(req.body.password)) {
            // add a line break if there is already a line in the message
            if (validationMessage.length > 0) {
                validationMessage += "\n";
            }
            validationMessage += "The password requires at least 5 uppercase, lowercase, AND numeric characters"
        }

        // make sure the email meets the length/format requirements
        if (!emailValid(req.body.email)) {
            // add a line break if there is already a line in the message
            if (validationMessage.length > 0) {
                validationMessage += "\n";
            }
            validationMessage += "Please provide a valid email address"
        }

        // if any of the input validation failed then error
        if (validationMessage.length > 0) {
            invalidInputError(done, validationMessage);
            return;
        } else {
            console.log("input validation passed");
        }

        // make sure the username and password aren't equal
        if (req.body.username === req.body.password) {
            usernameEqualsPasswordError(done);
            return;
        } else {
            console.log("username != password validation passed");
        }

        // make sure the username doesn't already exist
        cache.fetchField(req.body.username, "password")
            .then(function (pass) {

                if (!pass) {
                    // cache entry not found for username, proceed with registration

                    // write the password to the cache
                    cache.cacheWrite(req.body.username, "password", req.body.password)
                        .then(function (result) {
                            console.log("password written to cache with result: ", result);

                            if (result === 1) {
                                // got good result, write email to the cache
                                cache.cacheWrite(req.body.username, "email", req.body.email)
                                    .then(function (result) {

                                        console.log("email written to cache with result: ", result);
                                        if (result === 1) {
                                            // got good result, return success
                                            done({
                                                success: 1,
                                                result: {message: "registration successful for " + req.body.username}
                                            });

                                        }
                                    })
                                    .catch(function (err) {
                                        cacheWriteError(done);
                                    });
                            }
                        })
                        .catch(function (err) {
                            cacheWriteError(done);
                        });
                } else {

                    // cache hit with response
                    usernameExistsError(done);
                }
            })
            .catch(function (err) {
                //  cache read error
                cacheReadError(done);
            });

    } else {
        noBodyError(done);
    }
}

function loginValid(username, done) {
    console.log("loginValid got username ", username);

    // make sure the username exists
    return cache.fetchField(username, "password")
        .then(function (pass) {

            console.log("loginValid returning ", pass ? true : false);
            return pass ? true : false;
        })
        .catch(function (err) {
            cacheReadError(done);
        })
}

function emailValid(email) {

    // standard email regex
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(email);
}

function passwordValid(password) {

    // requires lowercase, uppercase, numbers, AND at least 5 characters
    let passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{5,})/;
    return passwordRegex.test(password);
}

function expirationDateValid(expirationDate) {

    // requires MM/YYYY
    let month = expirationDate.slice(0, 2);
    let slash = expirationDate.slice(2, 3);
    let year = expirationDate.slice(3)

    // number regex
    let regexMonth = /^[0-9]{2}$/;
    let regexYear = /^[0-9]{4}$/;

    let currentDate = new Date();
    let currentYear = currentDate.getYear() + 1900;

    return regexMonth.test(month) && regexYear.test(year) && year >= currentYear && slash === '/' && month < 13 && month > 0;
}

function accountNumberValid(accountNumber) {

    // requires 6-20 numbers
    let accountNumberRegex = /^[0-9]{6,20}$/;
    return accountNumberRegex.test(accountNumber);
}

function routingNumberValid(routingNumber) {

    // requires 9 numbers
    let routingNumberRegex = /^[0-9]{9}$/;
    return routingNumberRegex.test(routingNumber);
}

function cvvValid(cvv) {

    // requires 3 numbers
    let cvvRegex = /^[0-9]{3}$/;
    return cvvRegex.test(cvv);
}

function creditCardNumberValid(creditCardNumber) {

    // requires 16 numbers
    let creditCardNumberRegex = /^[0-9]{16}$/;
    return creditCardNumberRegex.test(creditCardNumber);
}

function usernameValid(username) {

    // requires at least 5 uppercase, lowercase, OR numeric characters
    let usernameRegex = /^((?=.*[a-z])|(?=.*[A-Z]|(?=.*[0-9])))(?=.{5,})/;
    return usernameRegex.test(username);
}

function zipValid(zip) {

    // requires 5 numeric characters
    let zipRegex = /^(?=.*[0-9]{5,})/;
    return zipRegex.test(zip);
}

function stateValid(state) {

    try {
        state = state[0].toUpperCase() + state.slice(1)
    } catch (err) {
        return false;
    }
    return states.indexOf(state) !== -1;
}

function noBodyError(done) {
    console.log("noBodyError function called");

    let errorResponse = {
        errorCode: 1,
        errorMessage: "No body provided in request",
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function noUsernamePasswordEmailError(done) {
    console.log("noUsernamePasswordEmailError function called");

    let errorResponse = {
        errorCode: 2,
        errorMessage: "No username/password/email provided in request ",
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function usernameEqualsPasswordError(done) {
    console.log("usernameEqualsPasswordError function called");

    let errorResponse = {
        errorCode: 3,
        errorMessage: "Username and password cannot be equal.",
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function cacheReadError(done) {
    console.log("cacheReadError function called");

    let errorResponse = {
        errorCode: 4,
        errorMessage: "Cache read error, please try again later",
        statusCode: 507
    };

    done({success: 0, err: {response: errorResponse}});
}

function usernameExistsError(done) {
    console.log("usernameExistsError function called");

    let errorResponse = {
        errorCode: 5,
        errorMessage: "Username already registered.  Please choose another.",
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function invalidInputError(done, message) {
    console.log("invalidInputError function called");

    let errorResponse = {
        errorCode: 6,
        errorMessage: message,
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function cacheWriteError(done) {
    console.log("cacheWriteError function called");

    let errorResponse = {
        errorCode: 7,
        errorMessage: "Cache write error, please try again later",
        statusCode: 507
    };

    done({success: 0, err: {response: errorResponse}});
}

function missingRequiredFieldsError(fieldsArray, done) {
    console.log("missingRequiredFieldsError function called");

    let errorResponse = {
        errorCode: 8,
        errorMessage: 'Please provide all required information for this request.  Required fields:  ' + fieldsArray,
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function userNotLoggedInError(done) {
    console.log("userNotLoggedInError function called");

    let errorResponse = {
        errorCode: 9,
        errorMessage: "User must be logged in to complete that request",
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function usernameNotExistsError(req, done) {
    console.log("usernameNotExistsError function called");

    let errorResponse = {
        errorCode: 10,
        errorMessage: "Username not found.  Would you like to register it?",
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function incorrectPasswordError(done) {
    console.log("incorrectPasswordError function called");

    let errorResponse = {
        errorCode: 11,
        errorMessage: "Incorrect password",
        statusCode: 400
    };

    done({success: 0, err: {response: errorResponse}});
}

function test(req, done) {
    console.log("userManagement test function called");
    done({success: 1, result: {message: "userManagement service operational"}});
}
