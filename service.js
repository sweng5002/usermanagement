const app_config_settings = require('configuration')().settings,
    seneca = require('seneca')({timeout: app_config_settings.get('/SENECA_TIMEOUT')}),
    clientInfo = app_config_settings.get('/microservicesClientInfo').userManagement;


seneca.use('userManagement.js');

seneca.listen({type:clientInfo.type, port:clientInfo.port });
